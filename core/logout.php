<?php
	session_start();

	unset($_SESSION["stat_login"]);
	unset($_SESSION["usernmae"]);
	unset($_SESSION["password"]);
	unset($_SESSION["level"]);

	session_destroy();
	header('Location: ../index.php');

?>