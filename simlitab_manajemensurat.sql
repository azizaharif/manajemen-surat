-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 02, 2019 at 03:57 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `simlitab_manajemensurat`
--

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
`id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(8) NOT NULL,
  `posisi` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`, `posisi`) VALUES
(1, 'admin', '123', 'admin'),
(2, 'ketua', '123', 'ketua'),
(18, 'pengabdian', '123', 'Koordinator Pengabdian'),
(20, 'penelitian', '123', 'Koordinator Penelitian'),
(21, 'ktu', '123', 'Kepala Tata Usaha'),
(22, 'sekretaris', '123', 'Sekretaris'),
(23, 'kasub pdi', '123', 'Kepala Sub Bagian PDI'),
(24, 'Kasub', '123', 'Kepala Sub Bagian Umum');

-- --------------------------------------------------------

--
-- Table structure for table `surat_disposisi`
--

CREATE TABLE IF NOT EXISTS `surat_disposisi` (
`id_suratd` int(11) NOT NULL,
  `s_nod` varchar(30) NOT NULL,
  `s_tgl_terima` date NOT NULL,
  `s_tgld` date NOT NULL,
  `s_kode` varchar(30) NOT NULL,
  `s_pengirimd` varchar(30) NOT NULL,
  `s_penerimad` varchar(30) NOT NULL,
  `s_disposisi` varchar(30) NOT NULL,
  `s_filed` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surat_disposisi`
--

INSERT INTO `surat_disposisi` (`id_suratd`, `s_nod`, `s_tgl_terima`, `s_tgld`, `s_kode`, `s_pengirimd`, `s_penerimad`, `s_disposisi`, `s_filed`) VALUES
(29, '04/HMIF/FT-UNRAM/2018', '2019-04-11', '2019-04-03', 'Biasa', 'jnet', 'Yedam', 'Baca ya', 'TEKAD TERBARU.xlsx'),
(30, '01/HMIF/FT-UNRAM/2018', '2019-02-27', '0000-00-00', 'Penting', 'treasure', 'haruto', 'Koor Penelitian', 'arif.jpg'),
(31, '05/HMIF/FT-UNRAM/2018', '2019-10-02', '2019-10-05', 'Penting', 'Ketua LPPM', 'Teknik', 'Penting', 'TEKAD TERBARU.xlsx'),
(32, '06/HMIF/FT-UNRAM/2018', '2019-10-02', '2019-10-09', 'penting', 'Ketua LPPM', 'Pengabdian', 'pengabdian', 'DATA DOSEN UNTUK SISTEM.xlsx');

-- --------------------------------------------------------

--
-- Table structure for table `surat_disposisi_user`
--

CREATE TABLE IF NOT EXISTS `surat_disposisi_user` (
  `id_surat_dis` int(11) NOT NULL,
  `s_no_dis` varchar(50) NOT NULL,
  `s_tgl_terima_dis` date NOT NULL,
  `s_tgl_dis` date NOT NULL,
  `s_kode_dis` varchar(30) NOT NULL,
  `s_pengirim_dis` varchar(30) NOT NULL,
  `s_penerima_dis` varchar(30) NOT NULL,
  `s_disposisi_dis` varchar(30) NOT NULL,
  `s_file_dis` varchar(30) NOT NULL,
  `s_status_dis` varchar(30) NOT NULL,
  `s_tujuan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surat_disposisi_user`
--

INSERT INTO `surat_disposisi_user` (`id_surat_dis`, `s_no_dis`, `s_tgl_terima_dis`, `s_tgl_dis`, `s_kode_dis`, `s_pengirim_dis`, `s_penerima_dis`, `s_disposisi_dis`, `s_file_dis`, `s_status_dis`, `s_tujuan`) VALUES
(1, '05/HMIF/FT-UNRAM/2018', '0000-00-00', '2019-10-02', '', 'Ketua LPPM', 'Teknik', '', 'TEKAD TERBARU.xlsx', 'Kepala Tata Usaha', 'Kepala Sub Bagian Umum'),
(26, '03/HMIF/FT-UNRAM/2018', '2019-04-03', '2019-02-27', 'Biasa', 'Sekret', 'ridho', 'Bacajajaja', '', 'Koor Penelitian', 'Kasub pdi'),
(32, '05/HMIF/FT-UNRAM/2018', '0000-00-00', '2019-10-02', '', 'Ketua LPPM', 'Teknik', '', 'TEKAD TERBARU.xlsx', 'Kepala Tata Usaha', 'Kepala Sub Bagian PDI'),
(33, '06/HMIF/FT-UNRAM/2018', '0000-00-00', '2019-10-02', '', 'Ketua LPPM', 'Pengabdian', '', 'DATA DOSEN UNTUK SISTEM.xlsx', 'Kepala Tata Usaha', 'Kepala Sub Bagian PDI'),
(34, '06/HMIF/FT-UNRAM/2018', '0000-00-00', '2019-10-02', '', 'Ketua LPPM', 'Pengabdian', '', 'DATA DOSEN UNTUK SISTEM.xlsx', 'Kepala Tata Usaha', 'Kepala Sub Bagian Umum');

-- --------------------------------------------------------

--
-- Table structure for table `surat_keluar`
--

CREATE TABLE IF NOT EXISTS `surat_keluar` (
`id_suratk` int(11) NOT NULL,
  `s_nok` varchar(50) NOT NULL,
  `s_tglk` date NOT NULL,
  `s_pengirimk` varchar(30) NOT NULL,
  `s_penerimak` varchar(30) NOT NULL,
  `s_perihalk` varchar(30) NOT NULL,
  `s_lampirank` int(11) NOT NULL,
  `s_filek` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surat_keluar`
--

INSERT INTO `surat_keluar` (`id_suratk`, `s_nok`, `s_tglk`, `s_pengirimk`, `s_penerimak`, `s_perihalk`, `s_lampirank`, `s_filek`) VALUES
(16, '01/HMIF/FT-UNRAM/2018', '2019-02-22', 'junkyu', 'haruto', 'undangan rapat', 4, 'Alat dan Bahan.docx'),
(17, '01/HMIF/FT-UNRAM/2018', '2019-02-27', 'yedam', 'jengwoo', 'undangan rapat', 1, '4 PKM-Kewirausahaan JAMAN ANDA');

-- --------------------------------------------------------

--
-- Table structure for table `surat_masuk`
--

CREATE TABLE IF NOT EXISTS `surat_masuk` (
`id_suratm` int(11) NOT NULL,
  `s_no` varchar(50) NOT NULL,
  `s_tgl` date NOT NULL,
  `s_pengirim` varchar(30) NOT NULL,
  `s_penerima` varchar(30) NOT NULL,
  `s_perihal` varchar(30) NOT NULL,
  `s_lampiran` int(11) NOT NULL,
  `s_file` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surat_masuk`
--

INSERT INTO `surat_masuk` (`id_suratm`, `s_no`, `s_tgl`, `s_pengirim`, `s_penerima`, `s_perihal`, `s_lampiran`, `s_file`) VALUES
(1, '01/HMIF/FT-UNRAM/2018', '2019-02-27', 'treasure', 'haruto', 'undangan rapat', 4, 'arif.jpg'),
(62, '05/LPPM-UNRAM/2018', '2019-04-03', 'Ketua LPPM UNRAM', 'Teknik UNRAM', 'Undangan Rapat', 3, 'tabel data base simlitabmas un'),
(63, '05/HMIF/FT-UNRAM/2018', '2019-10-02', 'Ketua LPPM', 'Teknik', 'Undangan Rapat', 6, 'TEKAD TERBARU.xlsx'),
(64, '06/HMIF/FT-UNRAM/2018', '2019-10-02', 'Ketua LPPM', 'Pengabdian', 'Undangan Rapat', 4, 'DATA DOSEN UNTUK SISTEM.xlsx');

-- --------------------------------------------------------

--
-- Table structure for table `surat_terkirim`
--

CREATE TABLE IF NOT EXISTS `surat_terkirim` (
`id_suratt` int(11) NOT NULL,
  `s_not` varchar(50) NOT NULL,
  `s_tgl-terimat` date NOT NULL,
  `s_tglt` date NOT NULL,
  `s_kodet` varchar(30) NOT NULL,
  `s_pengirimt` varchar(30) NOT NULL,
  `s_penerimat` varchar(30) NOT NULL,
  `s_disposisit` varchar(30) NOT NULL,
  `s_filet` varchar(30) NOT NULL,
  `s_statusfor` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surat_terkirim`
--

INSERT INTO `surat_terkirim` (`id_suratt`, `s_not`, `s_tgl-terimat`, `s_tglt`, `s_kodet`, `s_pengirimt`, `s_penerimat`, `s_disposisit`, `s_filet`, `s_statusfor`) VALUES
(30, '04/HMIF/FT-UNRAM/2018', '2019-04-03', '2019-04-11', 'Biasa', 'jnet', 'Yedam', 'Baca ya', 'TEKAD TERBARU.xlsx', 'Kasub pdi'),
(31, '04/HMIF/FT-UNRAM/2018', '2019-04-03', '2019-04-11', 'Biasa', 'jnet', 'Yedam', 'Baca ya', 'TEKAD TERBARU.xlsx', 'Koordinator Pengabdian'),
(32, '05/HMIF/FT-UNRAM/2018', '2019-10-05', '2019-10-02', 'Penting', 'Ketua LPPM', 'Teknik', 'Penting', 'TEKAD TERBARU.xlsx', 'Kepala Tata Usaha'),
(33, '06/HMIF/FT-UNRAM/2018', '2019-10-09', '2019-10-02', 'penting', 'Ketua LPPM', 'Pengabdian', 'pengabdian', 'DATA DOSEN UNTUK SISTEM.xlsx', 'Koordinator Pengabdian'),
(34, '06/HMIF/FT-UNRAM/2018', '2019-10-09', '2019-10-02', 'penting', 'Ketua LPPM', 'Pengabdian', 'penelitian', 'DATA DOSEN UNTUK SISTEM.xlsx', 'Koordinator Penelitian'),
(35, '06/HMIF/FT-UNRAM/2018', '2019-10-09', '2019-10-02', 'penting', 'Ketua LPPM', 'Pengabdian', 'pengabdian', 'DATA DOSEN UNTUK SISTEM.xlsx', 'Kepala Tata Usaha');

-- --------------------------------------------------------

--
-- Table structure for table `surat_terkirim_sekretaris`
--

CREATE TABLE IF NOT EXISTS `surat_terkirim_sekretaris` (
`id_surat_sekretaris` int(11) NOT NULL,
  `s_no_sekretaris` varchar(50) NOT NULL,
  `s_tgl_terima_sekretaris` date NOT NULL,
  `s_tgl_sekretaris` date NOT NULL,
  `s_kode_sekretaris` varchar(30) NOT NULL,
  `s_pengirim_sekretaris` varchar(30) NOT NULL,
  `s_penerima_sekretaris` varchar(30) NOT NULL,
  `s_disposisi_sekretaris` varchar(30) NOT NULL,
  `s_file_sekretaris` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surat_terkirim_sekretaris`
--

INSERT INTO `surat_terkirim_sekretaris` (`id_surat_sekretaris`, `s_no_sekretaris`, `s_tgl_terima_sekretaris`, `s_tgl_sekretaris`, `s_kode_sekretaris`, `s_pengirim_sekretaris`, `s_penerima_sekretaris`, `s_disposisi_sekretaris`, `s_file_sekretaris`) VALUES
(1, '01/HMIF/FT-UNRAM/2018', '0000-00-00', '0000-00-00', '', '', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `login`
--
ALTER TABLE `login`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surat_disposisi`
--
ALTER TABLE `surat_disposisi`
 ADD PRIMARY KEY (`id_suratd`);

--
-- Indexes for table `surat_disposisi_user`
--
ALTER TABLE `surat_disposisi_user`
 ADD PRIMARY KEY (`id_surat_dis`);

--
-- Indexes for table `surat_keluar`
--
ALTER TABLE `surat_keluar`
 ADD PRIMARY KEY (`id_suratk`);

--
-- Indexes for table `surat_masuk`
--
ALTER TABLE `surat_masuk`
 ADD PRIMARY KEY (`id_suratm`);

--
-- Indexes for table `surat_terkirim`
--
ALTER TABLE `surat_terkirim`
 ADD PRIMARY KEY (`id_suratt`);

--
-- Indexes for table `surat_terkirim_sekretaris`
--
ALTER TABLE `surat_terkirim_sekretaris`
 ADD PRIMARY KEY (`id_surat_sekretaris`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `surat_disposisi`
--
ALTER TABLE `surat_disposisi`
MODIFY `id_suratd` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `surat_keluar`
--
ALTER TABLE `surat_keluar`
MODIFY `id_suratk` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `surat_masuk`
--
ALTER TABLE `surat_masuk`
MODIFY `id_suratm` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `surat_terkirim`
--
ALTER TABLE `surat_terkirim`
MODIFY `id_suratt` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `surat_terkirim_sekretaris`
--
ALTER TABLE `surat_terkirim_sekretaris`
MODIFY `id_surat_sekretaris` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
