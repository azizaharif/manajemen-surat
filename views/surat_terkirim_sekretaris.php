<?php 
session_start();
if($_SESSION["stat_login"] == 1){

?>
<?php
	require_once "header-sekretaris.php";
?>
<?php
	include '../core/db_connection.php';
	$query1 = mysqli_query($conn, "SELECT *FROM surat_terkirim");
	// $query2 = mysqli_query($conn, "SELECT *FROM surat_terkirim_penelitian");
	// $query3 = mysqli_query($conn, "SELECT *FROM surat_terkirim_pengabdian");
	// $query4 = mysqli_query($conn, "SELECT *FROM surat_terkirim_ktu");
	// $query5 = mysqli_query($conn, "SELECT *FROM surat_terkirim_sekretaris");
	// $query6 = mysqli_query($conn, "SELECT *FROM surat_terkirim_kasubpdi");

	$count = mysqli_num_rows($query1);
	// $count1 = mysqli_num_rows($query2);
	// $count2 = mysqli_num_rows($query3);
	// $count3 = mysqli_num_rows($query4);
	// $count4 = mysqli_num_rows($query5);
	// $count5 = mysqli_num_rows($query6);
?>
	<wrapper>

	<div id="wrapper" class="page-width">
		
	<?php
		require_once "sidebar-sekretaris.php";
	?>
		<div id="containner">
			<div class="breadcrumbs">
				<ul class="breadcrumb">
				  <li><a href="#">Surat</a></li>
				  <li>Surat Terkirim</li>
				</ul>
			</div>
			<div class="main-containner">
				<table border="0px">
					
					<tr>
						<td class="paper-containner1">
							<table border="1px">
								<tr>
									<th class="no">No.</th>
									<th class="nomor-surat">Nomor Surat</th>
									<th class="tgl-terima">Tanggal Terima</th>
									<th class="tgl">Tanggal Surat</th>
									<th class="kode">Hal</th>
									<th class="pengirim-dis">Pengirim</th>
									<th class="penerima-dis">Penerima</th>
									<th class="disposisi">Disposisi</th>
									<th class="lamp">Lampiran</th>
									<th class="Categories">Tujuan</th>
								</tr>
								<?php
							$i=0;
								while ($result = mysqli_fetch_assoc($query1)) { ?>

								<tr>
									<td class="fornumb">
									  <?php 

									  $i+=1;
									 
									   echo $i;
									  ?>
									</td>
									<td><?php echo $result['s_not']; ?></td>
									<td><?php echo $result['s_tgl-terimat']; ?></td>
									<td><?php echo $result['s_tglt']; ?></td>
									<td><?php echo $result['s_kodet']; ?></td>
									<td><?php echo $result['s_pengirimt']; ?></td>
									<td><?php echo $result['s_penerimat']; ?></td>
									<td><?php echo $result['s_disposisit']; ?></td>
									<td id="download">
										 <a href="../assets/img/<?php echo $result['s_filet'];?>" target="s_filet"></a>
									</td>
									<td><?php echo $result['s_statusfor']; ?></td>
								</tr>

								<?php } ?>
								<tr>
									<th class="aksi" colspan="10">JUMLAH = <?php 
									echo $count;  ?> </th>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</wrapper>
<?php
	require_once "footer.php";
?>
<?php
}else{
	header('Location: ../index.php');
}

  ?>