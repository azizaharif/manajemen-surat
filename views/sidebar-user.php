<?php 
if($_SESSION["stat_login"] == 1){

?>

		<div id="sidebar-left">
			<div id="top-navigasi">
				<h1>NAVIGASI MENU</h1>
			</div>
			<div id="banner-navigasi">
				<img src="../assets/img/abu.png" id="banner-prof">
				<div class="log-session">
					<img class="profil" src="../assets/img/admin.png">
					<h1><?php echo $_SESSION["username"]; ?></h1>
					<h2><?php echo $_SESSION["level"]; ?></h2>
				</div>
			</div>
			<div id="menu-navigasi">
				<a href="panel-user.php" id="dashboard-menu">Dashboard</a>
				<a onclick="myFunction()" class="dropbtn" id="surat-menu">Surat</a>
				<div id="myDropdown" class="dropdown-content">
					<a href="surat-masuk-user.php" id="surat-masuk">Surat Masuk</a>
					<a href="surat_terkirim_user.php" id="surat-keluar">Surat Terkirim</a>
					<a href="surat-keluar-user.php" id="surat-keluar">Surat Keluar</a>
				</div>

				<a href="user-view-user.php" id="users-menu">Users</a>
				<a href="../core/logout.php" id="logout-menu">Logout</a>
			</div>
		</div>
<?php
}else{
	header('Location: ../index.php');
}

  ?>