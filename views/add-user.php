<?php 
session_start();
if($_SESSION["stat_login"] == 1){

?>
<?php
	require_once "header-admin.php";
?>
	<wrapper>

	<div id="wrapper" class="page-width">
		
	<?php
		require_once "sidebar-admin.php";
	?>
		<div id="containner">
			<div class="breadcrumbs">
				<ul class="breadcrumb">
				  <li><a href="#">Data Master</a></li>
				  <li>Profil Admin</li>
				</ul>
			</div>
			<div class="main-containner1">
				<table border="0px">
					<tr>
						<td class="title-containner1">
							<p>Tambah Pengguna (User)</p>
						</td>
					</tr>
					<tr>
						<td class="paper-containner2">
						<form action="../core/add-new-user.php" method="POST">
							<table border="0px">
								<tr>
									<td class="avatar">
									<h1>Profil Picture</h1>
										<img src="../assets/img/user.png"><br>
										<input type="file" name="avatar">

									</td>
									<td class="description-profil">
									
										<label>Level</label>
										  <select name="level">
										    <option value="-">-Pilih Level-</option>
										    <option value="ketua">Ketua</option>
										    <option value="Sekretaris">Sekretaris</option>
										    <option value="Koor Penelitian">Koordinator Penelitian</option>
										    <option value="Koor Pengabdian">Koordinator Pengabdian</option>
										    <option value="Ktu">Kepala Bagian Tata Usaha</option>
										     <option value="Kasub">Kepala Sub Bagian Umum</option>
										    <option value="Kasub pdi">Kepala Sub Bagian Program Data dan Informasi</option>

										  </select>
										<label>Username</label>
										<input type="text" placeholder="Enter username" name="username" required>
										<label>Password</label>
										<input type="password" placeholder="Enter Password" name="password" required>
										<button type="submit" class="add" name="btn">Tambah</button>

									<a href="#"><button class="back">Kembali</button>
									</a>
										
									</td>
								</tr>
							</table>
							</form>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</wrapper>
<?php
	require_once "footer.php";
?>
<?php
}else{
	header('Location: ../index.php');
}

  ?>