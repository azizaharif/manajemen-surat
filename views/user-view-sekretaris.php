<?php 
session_start();
if($_SESSION["stat_login"] == 1){

?>
<?php
	include '../core/db_connection.php';
	$query = mysqli_query($conn,"SELECT * FROM login");
?>
<?php
	require_once "header-user.php";
?>
	<wrapper>

	<div id="wrapper" class="page-width">
		
	<?php
		require_once "sidebar-user.php";
	?>
		<div id="containner">
			<div class="breadcrumbs">
				<ul class="breadcrumb">
				  <li><a href="#">Users</a></li>
				  <li>All Users</li>
				</ul>
			</div>
			<div class="main-containner">
				<table border="0px">
					
					<tr>
						<td class="paper-containner1">
							<table border="1px">
								<tr>
									<th class="no">No.</th>
									<th class="usernames">Username</th>
									<th class="divisi">Divisi</th>
								</tr>
							<?php
							$i=0;
								while ($result = mysqli_fetch_assoc($query)) { ?>	
								<tr>
									<td class="fornumb">
										
									<?php 

									  $i+=1;
									 
									   echo $i;
									  ?>

									</td>
									<td><?php echo $result['username']; ?></td>
									<td><?php 
										if($result['posisi']=='admin'){
											$data='Admin';
											
											echo $data;
										}else if($result['posisi']=='ketua'){
											$data='Ketua LPPM';
											
											echo $data;
										}else{echo $result['posisi']; }

									?></td>
								</tr>
								<?php } ?>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</wrapper>
<?php
	require_once "footer.php";
?>
<?php
}else{
	header('Location: ../index.php');
}

  ?>