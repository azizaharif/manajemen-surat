<?php 
session_start();
if($_SESSION["stat_login"] == 1){

?>
<?php
	require_once "header-admin.php";
?>
<?php
	include "../core/tampil.php";
?>
<?php
	include "../core/update.php";
	$query = mysqli_query($conn, "SELECT *FROM surat_masuk where id_suratm = $id");
	$result = mysqli_fetch_assoc($query);
?>
	<wrapper>

	<div id="wrapper" class="page-width">
		
	<?php
		require_once "sidebar-admin.php";
	?>
		<div id="containner">
			<div class="breadcrumbs">
				<ul class="breadcrumb">
				  <li>Id Surat : <?php echo $result['id_suratm']?></li>
				</ul>
			</div>
			<div class="main-containner1">
				<table border="0px">
					<tr>
						<td class="title-containner">
							<p>Surat Update</p>
						</td>
					</tr>
					<tr>
						<td class="paper-containner1">
						<div>

		    <div class="container1">
		    <div class="update-s">
		    	<form method="POST" enctype="multipart/form-data">
		     		  <label><b>Nomor Surat</b></label>
				      <input type="text" placeholder="Enter Nomor Surat" name="no-surat" 

				      value="<?php

							 echo $result['s_no']; ?>" 	
				      >
				      <label><b>Tanggal Surat</b></label>
				      <input type="text" placeholder="Enter Tanggal Surat" name="tgl-surat" 
				      value="<?php

							 echo $result['s_tgl']; ?>" 	
				      >
				      <label><b>Nama Pengirim</b></label>
				      <input type="text" placeholder="Enter Penerima" name="pengirim" value="<?php
							 echo $result['s_pengirim']; ?>">
				      <label><b>Nama Penerima</b></label>
				      <input type="text" placeholder="Enter Penerima" name="penerima" value="<?php
							 echo $result['s_penerima']; ?>">
				      <label><b>Perihal</b></label>
				      <input type="text" placeholder="Enter Perihal"  
				      name="perihal" 
				      value="<?php
							 echo $result['s_perihal']; ?>">
				      <label><b>Lampiran</b></label>
				      <input type="text" placeholder="Enter Perihal"  
				      name="lampiran" 
				      value="<?php
							 echo $result['s_lampiran']; ?>">
				      <label><b>File Lampiran</b></label>
				      <input type="file" name="file">
		    </div>
		    <div class="container3">
		      <button type="submit" class="save" name="submited">Simpan</button>
		      <a href="../views/surat-masuk-admin.php"><button type="button" class="cancelbtn">Batal</button>
		    </div>
		    </div>
		     
		</div>
						</td>
					</tr>
				</table>


			</div>
		</div>
	</div>
</wrapper>
<?php
	require_once "footer.php";
?>
<?php
}else{
	header('Location: ../index.php');
}

  ?>