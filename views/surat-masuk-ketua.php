<?php 
session_start();
if($_SESSION["stat_login"] == 1){
	
?>
<?php
	require_once "header-ketua.php";
?>
<?php
	include "../core/tampil.php";
	$query1 = mysqli_query($conn, "SELECT *FROM surat_disposisi");
	$count = mysqli_num_rows($query1);
?>
	<wrapper>

	<div id="wrapper" class="page-width">
		
	<?php
		require_once "sidebar-ketua.php";
	?>
		<div id="containner">
			<div class="breadcrumbs">
				<ul class="breadcrumb">
				  <li><a href="#">Surat</a></li>
				  <li><a href="#">Surat Masuk</a></li>
				  <li>inbox</li>
				</ul>
			</div>
			<div class="main-containner">
				<table border="0px">
					<tr>
						<td class="paper-containner1">
							<table border="1px">
								<tr>
									<th class="no">No.</th>
									<th class="nomor">Nomor Surat</th>
									<th class="tgl">Tanggal</th>
									<th class="pengirim">Pengirim</th>
									<th class="penerima">Penerima</th>
									<th class="prihal">Perihal</th>
									<th class="lamp">Lampiran</th>
									<th class="file">File</th>
									<th class="aksi" colspan="2">Aksi</th>
								</tr>
								<?php
							$i=0;
								while ($result = mysqli_fetch_assoc($query)) { ?>

								<tr>
									<td class="fornumb">
									  <?php 

									  $i+=1;
									 
									   echo $i;
									  ?>
									</td>
									<td><?php echo $result['s_no']; ?></td>
									<td><?php echo $result['s_tgl']; ?>
									</td>
									<td><?php echo $result['s_pengirim']; ?></td>
									<td><?php echo $result['s_penerima']; ?></td>
									<td><?php echo $result['s_perihal']; ?></td>
									<td><?php echo $result['s_lampiran']; ?></td>
									
									<td id="download">
										 <a href="../assets/img/<?php echo $result['s_file'];?>" target="s_file"></a>
									</td>

									<td id="edit" >

										<a href="surat-kirim-ketua.php?id=<?php echo $result['id_suratm'] ?>"
										>
										</a>

									</td>
								</tr>

								<?php } ?>
								<tr>
									<th class="aksi" colspan="15">JUMLAH = <?php 
									echo $count;  ?> </th>
								</tr>
								
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</wrapper>
<?php
	require_once "footer.php";
?>
<?php
}else{
	header('Location: ../index.php');
}

  ?>