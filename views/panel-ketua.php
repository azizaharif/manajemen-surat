<?php 
session_start();
if($_SESSION["stat_login"] == 1){

?>

<?php
	require_once "header-ketua.php";
?>
<?php
	include '../core/db_connection.php';
	$query1 = mysqli_query($conn, "SELECT *FROM surat_masuk");
	$query2 = mysqli_query($conn, "SELECT *FROM surat_keluar");
	$query3 = mysqli_query($conn, "SELECT *FROM login");

	$count = mysqli_num_rows($query1);
	$count1 = mysqli_num_rows($query2);
	$count2 = mysqli_num_rows($query3);
?>
	<wrapper>

	<div id="wrapper" class="page-width">
		
	<?php
		require_once "sidebar-ketua.php";
	?>
		<div id="containner">
			<div class="breadcrumbs">
				<ul class="breadcrumb">
				  <li><a href="#">Dashboard</a></li>
				  <li>Home</li>
				</ul>
			</div>
			<div class="main-containner">
				<table border="0px">
					<tr>
						<td class="title-containner">
							<p>Halaman Dashboard</p>
							<p class="paragraf">Hai&nbsp;<strong><?php echo $_SESSION["username"]; ?></strong>,&nbsp;Selamat Datang di Sistem Pengarsipan Manajemen Surat LPPM Universitas Mataram</p>
						</td>
					</tr>
					<tr>
						<td class="paper-containner">
							<table border="0px">
								<tr>
									<td>
										<div class="this_inbox">
											<div class="atas">
												<h1><?php 
									echo $count;  ?></h1>
											</div>
											<div class="bawah">
												<h1>Surat Masuk</h1>
											</div>
										</div>
									</td>
									<td>
										<div class="this_outbox">
											<div class="atas">
												<h1><?php 
									echo $count1;  ?></h1>
											</div>
											<div class="bawah">
												<h1>Surat keluar</h1>
											</div>
										</div>
									</td>
									<td>
										<div class="message-sent">
											<div class="atas">
												<h1><?php 
									echo $count+$count1;  ?></h1>
											</div>
											<div class="bawah">
												<h1>Total Surat</h1>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="users">
											<div class="atas">
												<h1><?php 
									echo $count2;  ?></h1>
											</div>
											<div class="bawah">
												<h1>Total Users</h1>
											</div>
										</div>
									</td>
									<td></td>
									<td></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</wrapper>
<?php
	require_once "footer.php";
?>

<?php
}else{
	header('Location: ../index.php');
}

  ?>