<?php 
session_start();
if($_SESSION["stat_login"] == 1){

?>
<?php
	include "../core/tampil.php";

	if (isset($_POST['cari'])) {

		$search = $_POST['pencarian'];
		$sql = mysqli_query($conn,"SELECT *FROM surat_keluar WHERE s_nok = '".$search."' ");

		$jumlah =mysqli_num_rows($sql);
?>
<?php
	require_once "header-admin.php";
?>
	<wrapper>

	<div id="wrapper" class="page-width">
		
	<?php
		require_once "sidebar-admin.php";
	?>
		<div id="containner">
			<div class="breadcrumbs">
				<ul class="breadcrumb">
				  <li><a href="#">Surat</a></li>
				  <li>Surat Keluar</li>
				</ul>
			</div>
			<div class="main-containner">
				<table border="0px">
					<tr>
						<td class="title-containner">
							<p>Surat Keluar</p>

							<div class="search">
								<form action="pencarian-surat-keluar.php" method="post">
									<input type="text" id="cari" placeholder="Cari data" name="pencarian">
									<button name="cari"></button>
								</form>	
							</div>
							
						</td>
					</tr>
					<tr>
						<td class="paper-containner1">
							<table border="1px">
								<tr>
									<th class="no">No.</th>
									<th class="nomor">Nomor Surat</th>
									<th class="tgl">Tanggal</th>
									<th class="pengirim">Pengirim</th>
									<th class="penerima">Penerima</th>
									<th class="prihal">Perihal</th>
									<th class="lamp">Lampiran</th>
									<th class="file">File</th>
								</tr>

					<?php if($jumlah > 0){ ?>			
							<?php
							$i=0;
								while ($result = mysqli_fetch_assoc($sql)) { ?>

								<tr>
									<td class="fornumb">
									  <?php 

									  $i+=1;
									 
									   echo $i;
									  ?>
									</td>
									<td><?php echo $result['s_nok']; ?></td>
									<td><?php echo $result['s_tglk']; ?>
									</td>
									<td><?php echo $result['s_pengirimk']; ?></td>
									<td><?php echo $result['s_penerimak']; ?></td>
									<td><?php echo $result['s_perihalk']; ?></td>
									<td><?php echo $result['s_lampirank']; ?></td>
									<td id="download">
										<a href="../assets/img/<?php echo $result['s_filek'];?>" target="s_filek"></a>

									</td>

								<?php } ?>
								<tr>
									<th class="aksi" colspan="10">JUMLAH = <?php 
									echo $jumlah;  ?> </th>
								</tr>
						<?php }else{ ?>
							<tr>
								<td colspan="8"><h3><br>Mohon Maaf ! Data tidak ditemukan<br><br></h3></td>

							</tr>

						<?php } ?>

							</table>
						</td>
					</tr>
				</table>


			</div>
		</div>
		
	</div>
</wrapper>

<?php
	require_once "footer.php";
?>

<?php		
	}

?>

<?php
}else{
	header('Location: ../index.php');
}

  ?>
	