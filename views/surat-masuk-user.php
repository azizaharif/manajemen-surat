<?php 
session_start();
	
if($_SESSION["stat_login"] == 1){
	$pengguna = $_SESSION["level"];

	include '../core/db_connection.php';

?>

<?php
	require_once "header-user.php";
?>
<?php
	include '../core/db_connection.php';
	$query1 = mysqli_query($conn, "SELECT *FROM surat_terkirim WHERE s_statusfor ='All Coordinator' or s_statusfor='".$pengguna."'");
	$query2 = mysqli_query($conn, "SELECT *FROM surat_disposisi_user WHERE s_tujuan ='All Coordinator' or s_tujuan='".$pengguna."'");
	
	$count = mysqli_num_rows($query1);
	$count2 = mysqli_num_rows($query2);
?>
	<wrapper>

	<div id="wrapper" class="page-width">
		
	<?php
		require_once "sidebar-user.php";
	?>
		<div id="containner">
			<div class="breadcrumbs">
				<ul class="breadcrumb">
				  <li><a href="#">Surat</a></li>
				  <li>Surat Masuk</li>
				</ul>
			</div>
			<div class="main-containner">
				<table border="0px">
		
					<tr>
						<td class="paper-containner1">
							<table border="1px">
								<tr>
									<th class="no">No.</th>
									<th class="nomor-surat">Nomor Surat</th>
									<th class="tgl-terima">Tanggal Terima</th>
									<th class="tgl">Tanggal Surat</th>
									<th class="kode"> Hal </th>
									<th class="pengirim-dis">Pengirim</th>
									<th class="penerima-dis">Penerima</th>
									<th class="disposisi">Disposisi</th>
									<th class="file">File</th>
									<th class="Disposer">Dari</th>
									<th class="aksi" colspan="2">Aksi</th>
								</tr>
							<?php
							$i=0;
								while ($result = mysqli_fetch_assoc($query1)) { ?>
								<tr>
									<td class="fornumb">
									<?php 

									  $i+=1;
									 
									   echo $i;
									  ?>	
									</td>
									<td><?php echo $result['s_not']; ?></td>
									<td><?php echo $result['s_tgl-terimat']; ?></td>
									<td><?php echo $result['s_tglt']; ?></td>
									<td><?php echo $result['s_kodet']; ?></td>
									<td><?php echo $result['s_pengirimt']; ?></td>
									<td><?php echo $result['s_penerimat']; ?></td>
									<td><?php echo $result['s_disposisit']; ?></td>
									<td id="download">
										 <a href="../assets/img/<?php echo $result['s_filet'];?>" target="s_file_t"></a>
									</td>
									<td>Sekretaris</td>
									<td id="edit" >

										<a href="surat_kirim_user.php?id=<?php echo $result['id_suratt'] ?>"
					>
										</a>

									</td>
								</tr>
								<?php } ?>
							<?php
							
								while ($result = mysqli_fetch_assoc($query2)) { ?>
								<tr>
									<td class="fornumb">
									<?php 

									  $i+=1;
									 
									   echo $i;
									  ?>	
									</td>
									<td><?php echo $result['s_no_dis']; ?></td>
									<td><?php echo $result['s_tgl_terima_dis']; ?></td>
									<td><?php echo $result['s_tgl_dis']; ?></td>
									<td><?php echo $result['s_kode_dis']; ?></td>
									<td><?php echo $result['s_pengirim_dis']; ?></td>
									<td><?php echo $result['s_penerima_dis']; ?></td>
									<td><?php echo $result['s_disposisi_dis']; ?></td>
									<td id="download">
										 <a href="../assets/img/<?php echo $result['s_file_dis'];?>" target="s_file_dis"></a>
									</td>
									<td><?php echo $result['s_status_dis']; ?></td>
									<td id="edit" >

										<a href="surat_kirim_user.php?id=<?php echo $result['id_surat_dis'] ?>"
										>
										</a>

									</td>
								</tr>
								<?php } ?>	
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</wrapper>
<?php
	require_once "footer.php";
?>
<?php
}else{
	header('Location: ../index.php');
}

  ?>