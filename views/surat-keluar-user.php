<?php 
session_start();
if($_SESSION["stat_login"] == 1){

?>
<?php
	require_once "header-user.php";
?>
<?php
	include "../core/ak-tampil.php";
	$query2 = mysqli_query($conn, "SELECT *FROM surat_keluar");
	$count1 = mysqli_num_rows($query2);
?>
	<wrapper>

	<div id="wrapper" class="page-width">
		
	<?php
		require_once "sidebar-user.php";
	?>
		<div id="containner">
			<div class="breadcrumbs">
				<ul class="breadcrumb">
				  <li><a href="#">Surat</a></li>
				  <li><a href="#">Surat Keluar</a></li>
				  <li>Outbox</li>
				</ul>
			</div>
			<div class="main-containner">
				<table border="0px">
					
					<tr>
						<td class="paper-containner1">
							<table border="1px">
								<tr>
									<th class="no">No.</th>
									<th class="nomor">Nomor Surat</th>
									<th class="tgl">Tanggal</th>
									<th class="pengirim">Pengirim</th>
									<th class="penerima">Penerima</th>
									<th class="prihal">Perihal</th>
									<th class="lamp">Lampiran</th>
									<th class="file">File</th>
									
								</tr>
						<?php
							$i=0;
								while ($result = mysqli_fetch_assoc($query)) { ?>

								<tr>
									<td class="fornumb">
									  <?php 

									  $i+=1;
									 
									   echo $i;
									  ?>
									</td>
									<td><?php echo $result['s_nok']; ?></td>
									<td><?php echo $result['s_tglk']; ?>
									</td>
									<td><?php echo $result['s_pengirimk']; ?></td>
									<td><?php echo $result['s_penerimak']; ?></td>
									<td><?php echo $result['s_perihalk']; ?></td>
									<td><?php echo $result['s_lampirank']; ?></td>
									<td id="download">
										<a href="../assets/img/<?php echo $result['s_filek'];?>" target="s_filek"></a>
									</td>
								</tr>

								<?php } ?>
								<tr>
									<th class="aksi" colspan="10">JUMLAH = <?php 
									echo $count1;  ?> </th>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</wrapper>
<?php
	require_once "footer.php";
?>
<?php
}else{
	header('Location: ../index.php');
}

  ?>