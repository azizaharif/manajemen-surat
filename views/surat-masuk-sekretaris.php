<?php 
session_start();
if($_SESSION["stat_login"] == 1){
	$pengguna = $_SESSION["username"];

?>
<?php
	require_once "header-sekretaris.php";
?>
<?php
	include "../core/tampil.php";
	$query1 = mysqli_query($conn, "SELECT *FROM surat_disposisi");
	$count = mysqli_num_rows($query1);
?>
	<!-- <wrapper> -->

	<div id="wrapper" class="page-width">
		
	<?php
	if($pengguna == "ketua"){
		require_once "sidebar-ketua.php";
	}
	else if($pengguna == "sekretaris"){
		require_once "sidebar-sekretaris.php";
	}
	else{
		require_once "sidebar-user";
	}
		
	?>
		<div id="containner">
			<div class="breadcrumbs">
				<ul class="breadcrumb">
				  <li><a href="#">Surat</a></li>
				  <li><a href="#">Surat Masuk</a></li>
				  <li>inbox</li>
				</ul>
			</div>
			<div class="main-containner">
				<table border="0px">
					<tr>
						<td class="paper-containner1">
							<table border="1px">
								<tr>
									<th class="no">No.</th>
									<th class="nomor-surat">Nomor Surat</th>
									<th class="tgl-terima">Tanggal Terima</th>
									<th class="tgl">Tanggal Surat</th>
									<th class="kode"> Hal </th>
									<th class="pengirim-dis">Pengirim</th>
									<th class="penerima-dis">Penerima</th>
									<th class="disposisi">Disposisi</th>
									<th class="file">File</th>
									<th class="aksi" colspan="2">Aksi</th>
								</tr>
								<?php
							$i=0;
								while ($result = mysqli_fetch_assoc($query1)) { ?>

								<tr>
									<td class="fornumb">
									  <?php 

									  $i+=1;
									 
									   echo $i;
									  ?>
									</td>
									<td><?php echo $result['s_nod']; ?></td>
									<td><?php echo $result['s_tgl_terima']; ?></td>
									<td><?php echo $result['s_tgld']; ?></td>
									<td><?php echo $result['s_kode']; ?></td>
									<td><?php echo $result['s_pengirimd']; ?></td>
									<td><?php echo $result['s_penerimad']; ?></td>
									<td><?php echo $result['s_disposisi']; ?></td>
									<td id="download">
										 <a href="../assets/img/<?php echo $result['s_filed'];?>" target="s_file"></a>
									</td>
									<td id="edit" >

										<a href="surat-kirim-disposisi.php?id=<?php echo $result['id_suratd'] ?>"
										>
										</a>

									</td>
								</tr>

								<?php } ?>
								<tr>
									<th class="aksi" colspan="15">JUMLAH = <?php 
									echo $count;  ?> </th>
								</tr>
								
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</wrapper>
<?php
	require_once "footer.php";
?>
<?php
}else{
	header('Location: ../index.php');
}

  ?>