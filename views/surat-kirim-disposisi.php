<?php 
session_start();
if($_SESSION["stat_login"] == 1){
	$id= $_GET['id'];
	$pengguna = $_SESSION["username"];
?>
<?php
	if($pengguna == "ketua"){
		require_once "header-ketua.php";
	}
	else if($pengguna == "sekretaris"){
		require_once "header-sekretaris.php";
	}
	else{
		require_once "header-user.php";
	}
?>
<?php
	include "../core/tampil.php";
?>
<?php
	// include "../core/surat-disposisi.php";
	$query = mysqli_query($conn, "SELECT *FROM surat_disposisi where id_suratd = '".$id."'");
	$query1 = mysqli_query($conn,"SELECT * FROM login");
	$result = mysqli_fetch_assoc($query);
?>
	<wrapper>

	<div id="wrapper" class="page-width">
		
	<?php
	if($pengguna == "ketua"){
		require_once "sidebar-ketua.php";
	}
	else if($pengguna == "sekretaris"){
		require_once "sidebar-sekretaris.php";
	}
	else{
		require_once "sidebar-user.php";
	}
		
	?>
		<div id="containner">
			<div class="breadcrumbs">
				<ul class="breadcrumb">
				  <li>Id User : <?php echo $id?></li>
				</ul>
			</div>
			<div class="main-containner1">
				<table border="0px">
					<tr>
						<td class="title-containner">
							<p>Disposisi Surat</p>
						</td>
						<td id="disposisi">
							<form method="post" action="../core/surat-disposisi.php" >
								<select name="disposisi"> 
								<option value="">-- Select One --</option>
								<option value="all"> All Coordinator</option>
								<?php while ($result1 = mysqli_fetch_assoc($query1)) {
									
								  ?>

								  <?php 
								  	if ($result1['posisi']!= 'admin' and $result1['posisi']!= 'ketua' ) {
									
								  ?>
								 	<option value="<?php echo $result1['posisi']; ?>" ><?php echo $result1['posisi']; ?></option> 
								  	<?php } ?>
								  <?php } ?>
								</select> 
						</td>
					</tr>
					<tr>
						<td class="paper-containner1" colspan="2">
						<div>

		    <div class="container1">
		    <div class="update-s">
		    	<!-- <form method="POST" enctype="multipart/form-data"> -->
		     		  <label><b>Nomor Surat</b></label>
				      <input type="text" placeholder="Enter Nomor Surat" name="no-surat" 
				      value="<?php

							 echo $result['s_nod']; ?>"	  	
				      >
				       <label><b>Tanggal Surat</b></label>
				      <input type="text" placeholder="Enter Perihal"  
				      name="tgl-surat" 
				      value="<?php
							 echo $result['s_tgld']; ?>"
					  >
					  <label><b>Tanggal Terima</b></label>
				      <input type="text" placeholder="Enter Perihal"  
				      name="tgl-terima" 
				      value="<?php
							 echo $result['s_tgl_terima']; ?>"
					  >
					  <label><b>Hal (Kode)</b></label>
				      <input type="text" placeholder="Enter Penerima" name="hal-kode" value="<?php
							 echo $result['s_kode']; ?>"
					  >
				      <label><b>Nama Pengirim</b></label>
				      <input type="text" placeholder="Enter Penerima" name="pengirim" value="<?php
							 echo $result['s_pengirimd']; ?>" 
					  >
				      <label><b>Nama Penerima</b></label>
				      <input type="text" placeholder="Enter Penerima" name="penerima" value="<?php
							 echo $result['s_penerimad']; ?>"
					  >
					  
					  <label><b>Disposisi</b></label>
				      <input type="text" placeholder="Enter Perihal"  
				      name="keterangan"
				      value="<?php
							 echo $result['s_disposisi']; ?>"
					  >
					  <label><b>File Lampiran</b></label>
				      <input  name="file"
				      value="<?php
							 echo $result['s_filed']; ?>" 
				      >
		    </div>

		    <div class="container3">
		      <button type="submit" class="save" name="submited">Simpan</button>
		      <a href="../views/surat-masuk-disposisi.php"><button type="button" class="cancelbtn">Batal</button></a>
		    </div>
		    </div>
		     
		</div>
						</td>
					</tr>
				</table>


			</div>
		</div>
	</div>
</wrapper>
<?php
	require_once "footer.php";
?>

<?php
}else{
	header('Location: ../index.php');
}

  ?>
