<?php 
session_start();
if($_SESSION["stat_login"] == 1){

?>
<?php
	require_once "header-admin.php";
?>
<?php
	include "../core/ak-tampil.php";
	$query2 = mysqli_query($conn, "SELECT *FROM surat_keluar");
	$count1 = mysqli_num_rows($query2);
?>
	<wrapper>

	<div id="wrapper" class="page-width">
		
	<?php
		require_once "sidebar-admin.php";
	?>
		<div id="containner">
			<div class="breadcrumbs">
				<ul class="breadcrumb">
				  <li><a href="#">Surat</a></li>
				  <li>Surat Keluar</li>
				</ul>
			</div>
			<div class="main-containner">
				<table border="0px">
					<tr>
						<td class="title-containner">
							<p>Surat Keluar</p>

							<div class="search">
								<form action="pencarian-surat-keluar.php" method="post">
									<input type="text" id="cari" placeholder="Cari data" name="pencarian">
									<button name="cari"></button>
								</form>	
								<a href="#" class="add-surat" onclick="document.getElementById('id01').style.display='block'">+</a>
								
							</div>
							
						</td>
					</tr>
					<tr>
						<td class="paper-containner1">
							<table border="1px">
								<tr>
									<th class="no">No.</th>
									<th class="nomor">Nomor Surat</th>
									<th class="tgl">Tanggal</th>
									<th class="pengirim">Pengirim</th>
									<th class="penerima">Penerima</th>
									<th class="prihal">Perihal</th>
									<th class="lamp">Lampiran</th>
									<th class="file">File</th>
									<th class="aksi" colspan="2">Aksi</th>
								</tr>
							<?php
							$i=0;
								while ($result = mysqli_fetch_assoc($query)) { ?>

								<tr>
									<td class="fornumb">
									  <?php 

									  $i+=1;
									 
									   echo $i;
									  ?>
									</td>
									<td><?php echo $result['s_nok']; ?></td>
									<td><?php echo $result['s_tglk']; ?>
									</td>
									<td><?php echo $result['s_pengirimk']; ?></td>
									<td><?php echo $result['s_penerimak']; ?></td>
									<td><?php echo $result['s_perihalk']; ?></td>
									<td><?php echo $result['s_lampirank']; ?></td>
									<td id="download">
										<a href="../assets/img/<?php echo $result['s_filek'];?>" target="s_filek"></a>

									</td>

									<td id="edit" >

										<a href="ak-surat-update-admin.php?id=<?php echo $result['id_suratk'] ?>"
										>
										</a>

									</td>
									<td id="delete">
										<a href="
										../core/ak-delete.php?id=<?php echo $result['id_suratk'];?>"></a>
									</td>
								</tr>

								<?php } ?>
								<tr>
									<th class="aksi" colspan="10">JUMLAH = <?php 
									echo $count1;  ?> </th>
								</tr>
							</table>
						</td>
					</tr>
				</table>


			</div>
		</div>
		<!-- The Modal -->
		<div id="id01" class="modal">

		  <!-- Modal Content -->
		  <form class="modal-content animate" action="../core/ak-insert.php" method="POST" enctype="multipart/form-data">

		    <div class="container1">
		      <label><b>Nomor Surat</b></label>
		      <input type="text" placeholder="Enter Nomor Surat" name="no-surat" required>
		      <label><b>Tanggal Surat</b></label>
		      <input type="text" placeholder="Enter Tanggal Surat" name="tgl-surat" required>
		      <label><b>Nama Pengirim</b></label>
		      <input type="text" placeholder="Enter Penerima" name="pengirim" required>
		      <label><b>Nama Penerima</b></label>
		      <input type="text" placeholder="Enter Penerima" name="penerima" required>
		      <label><b>Perihal</b></label>
		      <input type="text" placeholder="Enter Perihal"  
		      name="perihal" required>
		      <label><b>Lampiran</b></label>
		      <input type="text" placeholder="Enter Perihal"  
		      name="lampiran" required>
		      <label><b>File Lampiran</b></label>
		      <input type="file" name="file">
		    </div>
		    <div class="container2">
		      <button type="submit" class="save">Simpan</button>
		      <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Batal</button>
		    </div>
		  </div>
		</div>
	</form>
	</div>
</wrapper>
<script>
// Get the modal
var modal = document.getElementById('id01');
var modal = document.getElementById('id02');
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>
<?php
	require_once "footer.php";
?>

<?php
}else{
	header('Location: ../index.php');
}

  ?>