<!DOCTYPE html>
<html lang="en-us">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sistem Informasi Pengarsipan Surat LPPM Universitas Mataram</title>
	<link rel="stylesheet" type="text/css" href="../assets/css/tampilan.css">
	<link rel="stylesheet" type="text/css" href="../assets/fontawesome/css/font-awesome.min.css">
	<script type="text/javascript" src="../assets/js/surat.js"></script>
</head>
<body>
	<header>
		<div id="header-admin" class="page-width">
			<div id="header-left-admin">
				<a href="panel-ketua.php"><img src="../assets/img/logo unram.png"></a>
				<h1>SISTEM INFORMASI PENGARSIPAN SURAT LPPM UNIVERSITAS MATARAM</h1>
			</div>
			<div id="header-right-admin">
				<div id="login-as-name">
					<a href="#">Sekretaris LPPM Panel</a>
				</div>
			</div>
		</div>		
	</header>