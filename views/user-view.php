<?php 
session_start();
if($_SESSION["stat_login"] == 1){

?>
<?php
	include '../core/db_connection.php';
	$query = mysqli_query($conn,"SELECT * FROM login");
?>
<?php
	require_once "header-admin.php";
?>
	<wrapper>

	<div id="wrapper" class="page-width">
		
	<?php
		require_once "sidebar-admin.php";
	?>
		<div id="containner">
			<div class="breadcrumbs">
				<ul class="breadcrumb">
				  <li><a href="#">Users</a></li>
				  <li>All Users</li>
				</ul>
			</div>
			<div class="main-containner">
				<table border="0px">
					
					<tr>
						<td class="paper-containner1">
							<table border="1px">
								<tr>
									<th class="no">No.</th>
									<th class="usernames">Username</th>
									<th class="divisis">Divisi</th>
									
									<th class="aksi1" colspan="2">Aksi</th>
								</tr>
							<?php
							$i=0;
								while ($result = mysqli_fetch_assoc($query)) { ?>	
								<tr>
									<td class="fornumb">
										
									<?php 

									  $i+=1;
									 
									   echo $i;
									  ?>

									</td>
									<td><?php echo $result['username']; ?></td>
									<td><?php 
										if($result['posisi']=='admin'){
											$data='admin';
											
											echo $data;
										}else if($result['posisi']=='ketua'){
											$data='Ketua LPPM';
											
											echo $data;
										}else{echo $result['posisi']; }

									?></td>
									
									<td id="edit1">
										<a href="
										../views/update-data-user.php?id=<?php echo $result['id'];?>"></a>
									</td>
									<td id="delete1">
										<a href="
										../core/delete-user.php?id=<?php echo $result['id'];?>"></a>
									</td>
								</tr>
								<?php } ?>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</wrapper>
<?php
	require_once "footer.php";
?>
<?php
}else{
	header('Location: ../index.php');
}

  ?>